﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu2Controller : MonoBehaviour
{
    public Text highScore;
    public Text score;
    public void Start()
    {
        if (PlayerScore.playerScore > PlayerPrefs.GetFloat("HighScore", 0))
        {
            highScore.text = "NEW BEST SCORE!" + PlayerScore.playerScore;
        }
        else {
        highScore.text = "HIGH SCORE: " + PlayerPrefs.GetFloat("HighScore",0).ToString();
        score.text = "SCORE: " + PlayerScore.playerScore;
        }
    }

    public void LoadGame()
    {
        PlayerScore.playerScore = 0f;
        SceneManager.LoadScene("Game");
    }
    public void GetBack()
    {
        SceneManager.LoadScene("Start");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GetBack();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            LoadGame();
        }

       
    }
}
