﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private PlaneController plane;
    private int enemyHealth = 100;
    public fuel fuel;
    [SerializeField] GameObject _fuel;
    public GameObject explosion;
    public Transform shootPoint;
    public GameObject enemyBullet;
    [SerializeField] private float firerate;
    float timer = 1f;

    private bool dirRight = true;
    private float speed;
    private float moveRange;

    private void Awake()
    {
        speed = Random.Range(0.2f, 2f);
        moveRange = Random.Range(0.03f, 0.6f);
    }

    private void Start()
    {
        plane = FindObjectOfType<PlaneController>();
        //InvokeRepeating("EnemySpawner", 2f, 4f);
   
        // plane.gameObject
    }
    void Update()
    {
        

        timer += Time.deltaTime;
        if (transform.position.y < Camera.main.transform.position.y - 20)
        {
            Destroy(this.gameObject);
        }

        if (dirRight)
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        else
            transform.Translate(-Vector2.right * speed * Time.deltaTime);

        if (transform.position.x >= moveRange)
        {
            dirRight = false;
        }

        if (transform.position.x <= -moveRange)
        {
            dirRight = true;
        }
        if (transform.position.y < Camera.main.transform.position.y - 2)
        {
            Destroy(this.gameObject);
        }

    }

    public void EnemyTakeDamage(int damage)
    {
        enemyHealth -= damage;

        if (enemyHealth <= 0)
        {
            GameObject gp = Instantiate(explosion, transform.position, Quaternion.identity);
            
            Destroy(gameObject);  //niszczy ENEMY
            Destroy(gp, 1f);
        }
    }
    void EnemySpawner()
    {
        enemyBullet.GetComponent<EnemyBullet>().target = new Vector2(plane.transform.position.x, plane.transform.position.y) - new Vector2(transform.position.x, transform.position.y);
        GameObject bu = Instantiate(enemyBullet, shootPoint.position, Quaternion.Euler(0, 0, 100));
    }
}