﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlsController : MonoBehaviour
{
    public void BackTostart()
    {
        SceneManager.LoadScene("Start");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackTostart();
        }
    }
} 
