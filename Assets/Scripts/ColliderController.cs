﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


//zarządza kolizjami samolotu
public class ColliderController : MonoBehaviour
{
    
    public FuelLevelBar fuelLevelBar;

    protected float fuelLevel = 600f;
    protected float maxLevel = 1000f;
    public float FuelLevel { get { return fuelLevel; } }
    public float MaxLevel { get { return maxLevel; } }
    public Text fuelEnd;
    public Text scoreText;
    private int counter = 3;
    [SerializeField] private GameObject[] lives;

    private void Awake()
    {
        fuelLevel = maxLevel;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (fuelLevel > 0)
            {
                fuelLevel -= (Time.deltaTime * 10f) * 5f;
            }
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (fuelLevel > 0)
            {
                fuelLevel -= (Time.deltaTime * 10f) * -0.3f;
            }
            
        }
        if (fuelLevel > 0)
        {
            fuelLevel -= (Time.deltaTime * 10f);
        }
        

        if (fuelLevel <= 0)      //koniec paliwa
        {
            //fuelLevelBar.DisableUI();
            fuelEnd.text = "NO FUEL!";
            scoreText.text = "SCORE: " + PlayerScore.playerScore;
            Time.timeScale = 0.3f;
            Invoke("Restart", 1f);
            
        }
       
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("fueltag")) // fuel
        {
            
            fuelLevel = Mathf.Clamp(fuelLevel + 100, 0f, maxLevel);
            Destroy(collider.gameObject);
        }
        if (collider.gameObject.CompareTag("explosion"))  //explosion
        {
            if (PlayerScore.playerScore > 0)
            {
                PlayerScore.playerScore -= 20;
            }
        }
      
        if (collider.gameObject.CompareTag("enemy"))  //enemy
        {
            counter--;
            if (counter == 0)
            {
                fuelEnd.text = "YOU DIED";
                scoreText.text = "SCORE: " + PlayerScore.playerScore;
                lives[counter].SetActive(false);
                Time.timeScale = 0.3f;
                counter = 10;
                Invoke("Restart", 1f);
            }
            else lives[counter].SetActive(false);
           
            if (PlayerScore.playerScore > 0)
            {
                PlayerScore.playerScore -= 30;
            }
        }
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("restart");

    }
}