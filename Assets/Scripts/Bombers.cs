﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bombers : MonoBehaviour
{
    public Transform[] EnemyPlanesPointsMap;
    public GameObject[] EnemyPlanesPrefab;
    private List<Transform> bomber = new List<Transform>();
    public bool fly = false;

    public void Start()
    {
        HidePoints();
    }

    public void SpawnEnemyPlane(Transform trans)
    {
        GameObject BOMBER = Instantiate(EnemyPlanesPrefab[Random.Range(0, EnemyPlanesPrefab.Length)], trans.position, Quaternion.Euler(0,0,90));
        bomber.Add(BOMBER.transform);
    }

    void HidePoints()
    {
        for (int i = 0; i < EnemyPlanesPointsMap.Length; i++)
        {
            EnemyPlanesPointsMap[i].gameObject.SetActive(false);
        }
    }

    public void Fly()
    {
        fly = true;
    }

    private void Update()
    {
        if (fly == true)
        {
            for (int i = 0; i < bomber.Count; i++)
            {
               bomber[i].Translate(Vector3.left * Time.deltaTime * 1f, Space.World);
            }
        }
    }
   
}
