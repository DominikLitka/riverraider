﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fuel : MonoBehaviour
{
    private int health = 100;
    private Rigidbody2D rb;
    public GameObject explosion;
   
    void Update()
    {
        if (transform.position.y < Camera.main.transform.position.y - 2)
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage (int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        GameObject gp = Instantiate(explosion, transform.position, Quaternion.identity);   
        Destroy(gameObject);  
        Destroy(gp, 1f);
    }
}
