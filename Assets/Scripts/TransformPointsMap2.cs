﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPointsMap2 : MonoBehaviour
{

    public Transform[] FuelPoints;
    public GameObject fuelPrefab;
    float timer = 2f;
    private int spawnIndex;

    void Start()
    {
        HidePoints();
    }
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            for (int i = 0; i < 2; i++)
            {
                spawnIndex = Random.Range(0, FuelPoints.Length);
                GameObject go = Instantiate(fuelPrefab, FuelPoints[spawnIndex].position, Quaternion.identity);
            }
            timer = 3f;
        }
    }

    void HidePoints()
    {
        for (int i = 0; i < FuelPoints.Length; i++)
        {
            FuelPoints[i].gameObject.SetActive(false);
        }
    }

}
