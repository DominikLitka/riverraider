﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberTrigger : MonoBehaviour
{
    public Bombers bombers;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bombers.Fly();
        }
    }


}
