﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    public static float playerScore;
    private Text scoreText;

    void Start()
    {
        scoreText = GetComponent<Text>();

    }

    void Update()
    {
        scoreText.text = "SCORE: " + playerScore;
        if (playerScore > PlayerPrefs.GetFloat("HighScore", 0))
        {
            PlayerPrefs.SetFloat("HighScore", playerScore);
        }

    }
}
