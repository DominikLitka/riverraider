﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

public class TransformPoints : MonoBehaviour
{
    public Transform[] FuelPoints;
    public GameObject fuelPrefab;
    public Transform bridgePoint;
    public GameObject bridgePrefab;

    public void Start()
    {
        HidePoints();
       
    }

    public void SpawnFuel(Transform trans)
    {
        GameObject go = Instantiate(fuelPrefab, trans.position, Quaternion.identity);
       
    }
    public void SpawnBridge()
    {
        GameObject bridge = Instantiate(bridgePrefab, bridgePoint.position, Quaternion.identity);
    }

    void HidePoints()
    {
        for (int i = 0; i < FuelPoints.Length; i++)
        {
            FuelPoints[i].gameObject.SetActive(false);
        }
    }
}