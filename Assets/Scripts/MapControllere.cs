﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControllere : MonoBehaviour
{
    public GameObject[] mapPrefab;  //tablica map (gameobject)
    float move = 10.2f;
    private List<GameObject> mapList; //lista map
    private int mapIndex;
    
    void Start()
    {
        mapList = new List<GameObject>();  //inicjalizacja listy map
        CreateMap();
    }

    void Update()
    {
        DestroyMap();
    }

    void CreateMap()
    {
        mapIndex = 0;

        for (int i = 0; i < 3; i++)
        {
            InstantiateMap();
        }
    }

    private void InstantiateMap()
    {
        Vector3 position = new Vector3(0, 0, 0);
        position.y = move * mapIndex;
        GameObject go = Instantiate(mapPrefab[mapIndex % 2], position, Quaternion.identity);

        //fuel spawn
        TransformPoints tranPoints = go.GetComponent<TransformPoints>();
        if (tranPoints != null)
        {
            List<Transform> points = new List<Transform>(tranPoints.FuelPoints);
            for (int i = 0; i < 2; i++)
            {
                int index = Random.Range(0, points.Count);

                tranPoints.SpawnFuel(points[index]);
                points.RemoveAt(index);
            }
        }
        //enemy spawn
        EnemyPoints enemy = go.GetComponent<EnemyPoints>();
        if (tranPoints != null)
        {
            List<Transform> enemyPoints = new List<Transform>(enemy.EnemyPointsMap);
            for (int i = 0; i < 7; i++)
            {
                int index = Random.Range(0, enemyPoints.Count);

                enemy.SpawnEnemy(enemyPoints[index]);       //przekazuje wylosowany punkt do metody SpawnEnemy
                enemyPoints.RemoveAt(index);
            }
        }

        //bomber spawn
        Bombers bombers = go.GetComponent<Bombers>();
        List<Transform> bomberPoints = new List<Transform>(bombers.EnemyPlanesPointsMap);
        for (int i = 0; i < 3; i++)
        {
            int index = 0; 

            bombers.SpawnEnemyPlane(bomberPoints[index]);
            bomberPoints.RemoveAt(index);
            index++;
        }


        tranPoints.SpawnBridge();  //bridge spawn
        mapList.Add(go);
        mapIndex++;
    }

    void DestroyMap()
    {
        if (Camera.main.gameObject.transform.position.y > (mapList[2].transform.position.y - 11.5f))
        {
            Destroy(mapList[0]);

            mapList.RemoveAt(0);
            InstantiateMap();
        }
    }
 }