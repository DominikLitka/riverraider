﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallExplosionScript : MonoBehaviour
{
  public void DestroyExplosion()
    {
        Destroy(gameObject);
    }
}
