﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FuelLevelBar : MonoBehaviour
{
    public ColliderController colliderController;
  
    public Slider FuelLevelSlider;
    public float CurrentLevel;
    public int CurrentPoint;

    public Text FuelText;
    protected float maxLevel;

    void Start()
    {
        CurrentLevel = colliderController.FuelLevel;
        maxLevel = colliderController.FuelLevel;
        CurrentLevel = maxLevel;
    }

    void Update()
    {
        CurrentLevel = colliderController.FuelLevel;

        FuelLevelSlider.maxValue = maxLevel;
        FuelLevelSlider.value = CurrentLevel;
        FuelText.text = Mathf.CeilToInt(CurrentLevel) + "/" + maxLevel.ToString() + " FUEL";
       
    }

    public void DisableUI()
    {
        FuelText.GetComponent<Text>().enabled = false;
        FuelLevelSlider.GetComponent<Slider>().enabled = false;
    }
}
