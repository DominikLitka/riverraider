﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour
{
    public GameObject exp;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
            Instantiate(exp, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("bullet"))
        {

            GameObject explo =  Instantiate(exp, transform.position, Quaternion.identity);
            Destroy(gameObject);
            Destroy(explo, 1);
        }
    }
}
