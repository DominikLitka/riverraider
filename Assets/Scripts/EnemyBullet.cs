﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public Vector2 target;
    public int damagePlane = 100;
    public float speedBullet = 5f;
    public GameObject smallExplosion;

    void Update()
    {
         transform.position += new Vector3( Time.deltaTime * target.normalized.x * speedBullet, Time.deltaTime * target.normalized.y * speedBullet, 0);
        Destroy(gameObject, 6f);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BoxCollider"))
        {
            Debug.Log(collision.transform.name);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Instantiate(smallExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject, 0.06f);
        }
        if (collision.gameObject.CompareTag("bullet"))
        {
            Instantiate(smallExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
} 