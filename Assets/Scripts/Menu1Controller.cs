﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu1Controller : MonoBehaviour
{
    public Text highScore;
    private void Start()
    {
        highScore.text = "HIGH SCORE: " + PlayerPrefs.GetFloat("HighScore", 0).ToString();
    }
    public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void QuitGame()
    {
        Application.Quit();

    }
    public void ControlsMenu()
    {
        SceneManager.LoadScene("Controls");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("quit");
            QuitGame();

        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            LoadGame();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ControlsMenu();
        }
    }


}