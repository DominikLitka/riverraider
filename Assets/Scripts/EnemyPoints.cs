﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

public class EnemyPoints : MonoBehaviour
{
    public Transform[] EnemyPointsMap;
    public GameObject[] EnemyPrefab;

    public void Start()
    {
        HidePoints();
    }

    public void SpawnEnemy(Transform trans)
    {
        //spawn enemy losowo wybranego z tablicy 

        GameObject enemy = Instantiate(EnemyPrefab[Random.Range(0, EnemyPrefab.Length)], trans.position, Quaternion.identity);

        //GameObject go = Instantiate(EnemyPrefab, trans.position, Quaternion.identity);
    }

    void HidePoints()
    {
        for (int i = 0; i < EnemyPointsMap.Length; i++)
        {
            EnemyPointsMap[i].gameObject.SetActive(false);
        }
    }
}
