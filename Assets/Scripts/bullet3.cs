﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet3 : MonoBehaviour
{
    public int damageFuel = 100;
    private float speed = 8f;
    public Rigidbody2D rb;
    public int KillPoints = 100;

    void Start()
    {
        rb.velocity = transform.up * speed;
    }

    void Update()
    {
        if (transform.position.y > Camera.main.transform.position.y + 20)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        fuel fuel = hitInfo.GetComponent<fuel>();
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        PlaneController planeController = /*hitInfo.GetComponent<PlaneController>();*/FindObjectOfType<PlaneController>();

        if (hitInfo.gameObject.CompareTag("fueltag"))
        {
            fuel.TakeDamage(damageFuel);
            Destroy(gameObject); //niszczy pocisk w kontakcie z FUEL
        }
        if (hitInfo.gameObject.CompareTag("enemy"))
        {
            enemy.EnemyTakeDamage(damageFuel);  //zabija enemy

            PlayerScore.playerScore += KillPoints;   //dodaje 100 punktów do pola player.score

            Destroy(gameObject);
        }
        if (hitInfo.gameObject.CompareTag("enemyBullet"))
        {
            Destroy(gameObject);
        }
        if (hitInfo.gameObject.CompareTag("bridge"))
        {
            Destroy(gameObject);
        }
        if (transform.position.y > Camera.main.transform.position.y + 1.5f)
        {
            Destroy(gameObject);
        }
       
    }
}