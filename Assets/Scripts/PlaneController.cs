﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour
{
    private float Speed = 1f;
    public GameObject player;
    private Rigidbody2D rigidb;
    public float xAxisValue;


    private void Start()
    {
        rigidb = GetComponent<Rigidbody2D>();
        
    }

    void Update()
    {
        float hAxis = Input.GetAxis("Vertical");
        float xAxis = Input.GetAxis("Horizontal");
        xAxisValue = xAxis;
        Vector3 movment = new Vector3(xAxis, Mathf.Clamp(hAxis, 0, 1), 0) * Speed * Time.deltaTime;
        rigidb.velocity = movment * 110f;

       
    }
}
