﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class RotationPlane : MonoBehaviour
{

    //public PlaneController planeController;
    //private Animator myAnimator;
    private Quaternion noneRotation;
    private Quaternion leftRotation;
    private Quaternion rightRotation;

    [SerializeField] private RotateType currentType = RotateType.None;

    private enum RotateType
    {
        None,
        Left,
        Right,
    }

    private void Start()
    {
        noneRotation = Quaternion.Euler(new Vector3(0f, 270f, 90f));
        leftRotation = Quaternion.Euler(new Vector3(0f, 320f, 90f));
        rightRotation = Quaternion.Euler(new Vector3(0f, 220f, 90f));
    }

    private void Update()
    {
        //myAnimator.SetFloat("Turn", planeController.xAxisValue);

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            currentType = RotateType.Left;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            currentType = RotateType.Right;
        } else
        {
            currentType = RotateType.None;
        }

        switch(currentType)
        {
            case RotateType.None:
                transform.rotation = Quaternion.Slerp(transform.rotation, noneRotation, Time.deltaTime * 10f);
                break;
            case RotateType.Left:
                transform.rotation = Quaternion.Slerp(transform.rotation, leftRotation, Time.deltaTime * 10f);
                break;
            case RotateType.Right:
                transform.rotation = Quaternion.Slerp(transform.rotation, rightRotation, Time.deltaTime * 10f);
                break;
        }
    }
}